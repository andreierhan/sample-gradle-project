package com.example;

public class Calculator {

    public String calculateExpression(Expression expression) {
        switch (expression.getOperation()) {
            case "+" :
                return this.executeAddition(expression);
            case "-" :
                return this.executeSubtraction(expression);
            case "*" :
                return this.executeMultiplication(expression);
            case "/" :
                return this.executeDivision(expression);
            default :
                return "This operation is invalid!";
        }
    }

    public String executeAddition(Expression expression) {
        double firstNumber;
        double secondNumber;
        try{
            firstNumber = Double.parseDouble(expression.getFirstNumber());
            secondNumber = Double.parseDouble(expression.getSecondNumber());
            return String.valueOf(firstNumber + secondNumber);
        }
        catch (NumberFormatException ex){
            System.out.println("At least one operand is not a number!");
            return null;
        }
    }

    public String executeSubtraction(Expression expression) {
        double firstNumber;
        double secondNumber;
        try{
            firstNumber = Double.parseDouble(expression.getFirstNumber());
            secondNumber = Double.parseDouble(expression.getSecondNumber());
            return String.valueOf(firstNumber - secondNumber);
        }
        catch (NumberFormatException ex){
            System.out.println("At least one operand is not a number!");
            return null;
        }
    }

    public String executeMultiplication(Expression expression) {
        double firstNumber;
        double secondNumber;
        try{
            firstNumber = Double.parseDouble(expression.getFirstNumber());
            secondNumber = Double.parseDouble(expression.getSecondNumber());
            return String.valueOf(firstNumber * secondNumber);
        }
        catch (NumberFormatException ex){
            System.out.println("At least one operand is not a number!");
            return null;
        }
    }

    public String executeDivision(Expression expression) {
        double firstNumber;
        double secondNumber;
        try{
            firstNumber = Double.parseDouble(expression.getFirstNumber());
            secondNumber = Double.parseDouble(expression.getSecondNumber());
            return String.valueOf(firstNumber / secondNumber);
        }
        catch (NumberFormatException ex){
            System.out.println("At least one operand is not a number!");
            return null;
        }
    }

}
