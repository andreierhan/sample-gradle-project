package com.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Read first number: ");
        String firstNumber = scanner.nextLine();
        System.out.print("Read operation: ");
        String operation = scanner.nextLine();
        System.out.print("Read second number: ");
        String secondNumber = scanner.nextLine();
        Expression expression = new Expression();
        expression.setFirstNumber(firstNumber);
        expression.setOperation(operation);
        expression.setSecondNumber(secondNumber);
        Calculator calculator = new Calculator();
        String result = calculator.calculateExpression(expression);
        if(result != null && !result.equals("This operation is invalid!")) {
            System.out.println("The result of this expression is: " + result);
        }else if(result != null){
            System.out.println(result);
        }
        scanner.close();
    }
}