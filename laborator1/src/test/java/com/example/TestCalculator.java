package com.example;
import org.junit.jupiter.api.Test;

import com.example.Calculator;
import com.example.Expression;

import static org.junit.jupiter.api.Assertions.assertNull;

public class TestCalculator {

    @Test
    public void executeAdditionWithInvalidNumbers(){
        Expression expression = new Expression();
        expression.setOperation("+");
        expression.setFirstNumber("d4ds");
        expression.setSecondNumber("fd23e");
        Calculator calculator = new Calculator();
        String firstResult = calculator.calculateExpression(expression);
        assertNull(firstResult);

        expression.setFirstNumber("12sds");
        expression.setSecondNumber("4.434ddf");
        String secondResult = calculator.calculateExpression(expression);
        assertNull(secondResult);

        expression.setFirstNumber("12");
        expression.setSecondNumber("12ds");
        String thirdResult = calculator.calculateExpression(expression);
        assertNull(thirdResult);

    }

}
